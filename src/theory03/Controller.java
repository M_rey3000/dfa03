//<editor-fold desc="Description">
package theory03;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

import javax.swing.*;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Controller implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    private Circle cir_q0;
    @FXML
    private Circle cir_q1;
    @FXML
    private Circle cir_q2;
    @FXML
    private Text txt_q0q0;
    @FXML
    private Text txt_q0q1;
    @FXML
    private Text txt_q1q1;
    @FXML
    private Text txt_q1q2;
    @FXML
    private Text txt_q2q0;
    @FXML
    private Text txt_q2q2;
    @FXML
    private Button btnInput;

    private int cnt_q0q0 = 0;
    private int cnt_q0q1 = 0;
    private int cnt_q1q1 = 0;
    private int cnt_q1q2 = 0;
    private int cnt_q2q0 = 0;
    private int cnt_q2q2 = 0;

    @FXML
    private void btnInputAction(ActionEvent event) {
        cnt_q0q0 = 0;
        cnt_q0q1 = 0;
        cnt_q1q1 = 0;
        cnt_q1q2 = 0;
        cnt_q2q0 = 0;
        cnt_q2q2 = 0;

        machine();
    }


    private void machine() {
        String state = "q0";
        Scanner scan = new Scanner(System.in);
//        System.out.println(">>>Input: ");
        String strInput = JOptionPane.showInputDialog(null, ">>>Input: ", "Input", JOptionPane.QUESTION_MESSAGE);

        for (int i = 0; i < strInput.length(); i++) {
            switch (state) {
                case "q0":
                    if (strInput.charAt(i) == 'a') {
                        state = "q1";
                        cnt_q0q1++;
                    } else if (strInput.charAt(i) == 'b') {
                        state = "q0";
                        cnt_q0q0++;
                    } else {
                        state = "trap";
                    }

                    break;

                case "q1":
                    if (strInput.charAt(i) == 'a') {
                        state = "q2";
                        cnt_q1q2++;
                    } else if (strInput.charAt(i) == 'b') {
                        state = "q1";
                        cnt_q1q1++;
                    } else {
                        state = "trap";
                    }
                    break;

                case "q2":
                    if (strInput.charAt(i) == 'a') {
                        state = "q0";
                        cnt_q2q0++;
                    } else if (strInput.charAt(i) == 'b') {
                        state = "q2";
                        cnt_q2q2++;
                    } else {
                        state = "trap";
                    }
                    break;


            }

        }
        switch (state) {
            //Accepted
            case "q2":
                JOptionPane.showMessageDialog(null, strInput, "Accepted!", JOptionPane.INFORMATION_MESSAGE);
                cir_q0.setFill(Color.DODGERBLUE);
                cir_q1.setFill(Color.DODGERBLUE);
                cir_q2.setFill(Color.BLUEVIOLET);
                setAcceptInfo();
                break;
            //Not Accepted
            case "q1":
                JOptionPane.showMessageDialog(null, strInput, "NOT Accepted!", JOptionPane.ERROR_MESSAGE);
                cir_q0.setFill(Color.DODGERBLUE);
                cir_q1.setFill(Color.RED);
                cir_q2.setFill(Color.DODGERBLUE);
                break;
            case "q0":
                JOptionPane.showMessageDialog(null, strInput, "NOT Accepted!", JOptionPane.ERROR_MESSAGE);
                cir_q0.setFill(Color.RED);
                cir_q1.setFill(Color.DODGERBLUE);
                cir_q2.setFill(Color.DODGERBLUE);
                break;
            default:
                JOptionPane.showMessageDialog(null, "Not Accepted! (Alphabet is (a,b))", "", JOptionPane.ERROR_MESSAGE);
                cir_q0.setFill(Color.DODGERBLUE);
                cir_q1.setFill(Color.DODGERBLUE);
                cir_q2.setFill(Color.DODGERBLUE);
                break;
        }

    }

    private void setAcceptInfo() {
        txt_q0q0.setText("b: " + cnt_q0q0);
        txt_q0q1.setText("a: " + cnt_q0q1);
        txt_q1q1.setText("b: " + cnt_q1q1);
        txt_q1q2.setText("a: " + cnt_q1q2);
        txt_q2q0.setText("a: " + cnt_q2q0);
        txt_q2q2.setText("b: " + cnt_q2q2);
    }

}
//</editor-fold>
