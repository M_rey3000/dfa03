package theory03;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
/* DFA on a,b with L={W : Na(W) mod 3 > 1}
Slide CH2 page 43
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("DFA on a,b with L={W : Na(W) mod 3 > 1}");
        primaryStage.setScene(new Scene(root, 550, 365));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
